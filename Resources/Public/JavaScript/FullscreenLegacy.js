define(['jquery'], function ($) {
    const Fullscreen = {};

    function launchIntoFullscreen (element) {
        if (element.requestFullscreen) {
            element.requestFullscreen();
        } else if (element.mozRequestFullScreen) {
            element.mozRequestFullScreen();
        } else if (element.webkitRequestFullscreen) {
            element.webkitRequestFullscreen();
        } else if (element.msRequestFullscreen) {
            element.msRequestFullscreen();
        }
    }

    function exitFullscreen () {
        if (document.exitFullscreen) {
            document.exitFullscreen();
        } else if (document.mozCancelFullScreen) {
            document.mozCancelFullScreen();
        } else if (document.webkitExitFullscreen) {
            document.webkitExitFullscreen();
        }
    }

    Fullscreen.init = function () {
        const fullscreenActivateButton = $('#codingms-fullscreen-hooks-backend-toolbar-fullscreentoolbaritem .activate')
        const fullscreenDeactivateButton = $('#codingms-fullscreen-hooks-backend-toolbar-fullscreentoolbaritem .deactivate')

        document.addEventListener("fullscreenchange", function( event ) {

            const fullscreenElement = document.fullscreenElement || document.mozFullScreenElement || document.webkitFullscreenElement;
            if ( fullscreenElement ) {
                fullscreenDeactivateButton.css({display: ''})
                fullscreenActivateButton.css({display: 'none'})
            } else {
                fullscreenDeactivateButton.css({display: 'none'})
                fullscreenActivateButton.css({display: ''})
            }

        });

        fullscreenActivateButton.click(function(){
            launchIntoFullscreen(document.documentElement)
            $(this).css({display: 'none'})
            fullscreenDeactivateButton.css({display: ''})
        })
        fullscreenDeactivateButton.click(function(){
            exitFullscreen(document.documentElement)
            $(this).css({display: 'none'})
            fullscreenActivateButton.css({display: ''})
        })
    };

    Fullscreen.init();

    return Fullscreen;
});
