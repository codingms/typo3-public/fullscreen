import DocumentService from "@typo3/core/document-service.js";
import jQuery from "jquery";

class Fullscreen {

    init() {
        DocumentService.ready().then(() => {
            const _this = this;
            const fullscreenActivateButton = jQuery('#codingms-fullscreen-hooks-backend-toolbar-fullscreentoolbaritem .activate')
            const fullscreenDeactivateButton = jQuery('#codingms-fullscreen-hooks-backend-toolbar-fullscreentoolbaritem .deactivate')

            document.addEventListener("fullscreenchange", function( event ) {

                const fullscreenElement = document.fullscreenElement || document.mozFullScreenElement || document.webkitFullscreenElement;
                if ( fullscreenElement ) {
                    fullscreenDeactivateButton.css({display: ''})
                    fullscreenActivateButton.css({display: 'none'})
                } else {
                    fullscreenDeactivateButton.css({display: 'none'})
                    fullscreenActivateButton.css({display: ''})
                }

            });

            fullscreenActivateButton.click(function(){
                _this.launchIntoFullscreen(document.documentElement)
                jQuery(this).css({display: 'none'})
                fullscreenDeactivateButton.css({display: ''})
            })
            fullscreenDeactivateButton.click(function(){
                _this.exitFullscreen(document.documentElement)
                jQuery(this).css({display: 'none'})
                fullscreenActivateButton.css({display: ''})
            })
        });
    }

    launchIntoFullscreen (element) {
        if (element.requestFullscreen) {
            element.requestFullscreen();
        } else if (element.mozRequestFullScreen) {
            element.mozRequestFullScreen();
        } else if (element.webkitRequestFullscreen) {
            element.webkitRequestFullscreen();
        } else if (element.msRequestFullscreen) {
            element.msRequestFullscreen();
        }
    }

    exitFullscreen () {
        if (document.exitFullscreen) {
            document.exitFullscreen();
        } else if (document.mozCancelFullScreen) {
            document.mozCancelFullScreen();
        } else if (document.webkitExitFullscreen) {
            document.webkitExitFullscreen();
        }
    }
}

export default new Fullscreen();
