<?php declare(strict_types=1);

namespace CodingMs\Fullscreen\Hooks\Backend\Toolbar;

use Psr\Http\Message\ServerRequestInterface;
use TYPO3\CMS\Backend\Toolbar\RequestAwareToolbarItemInterface;
use TYPO3\CMS\Backend\Toolbar\ToolbarItemInterface;
use TYPO3\CMS\Core\Page\JavaScriptModuleInstruction;
use TYPO3\CMS\Core\Page\PageRenderer;
use TYPO3\CMS\Core\Utility\GeneralUtility;
use TYPO3\CMS\Extbase\Persistence\QueryResultInterface;
use TYPO3\CMS\Backend\View\BackendViewFactory;

class FullscreenToolbarItem implements ToolbarItemInterface, RequestAwareToolbarItemInterface
{
    /**
     * @var QueryResultInterface|null
     */
    protected ?QueryResultInterface $availableUsers = null;

    /**
     * @var ServerRequestInterface
     */
    private ServerRequestInterface $request;

    /**
     * @var BackendViewFactory
     */
    private BackendViewFactory $backendViewFactory;

    /**
     * @param BackendViewFactory $backendViewFactory
     */
    public function __construct(BackendViewFactory $backendViewFactory)
    {
        $this->backendViewFactory = $backendViewFactory;

        /** @var PageRenderer $pageRenderer */
        $pageRenderer = GeneralUtility::makeInstance(PageRenderer::class);
        $pageRenderer->getJavaScriptRenderer()->addJavaScriptModuleInstruction(
            JavaScriptModuleInstruction::create('@codingms/fullscreen/Fullscreen.js')->invoke('init')
        );
    }


    /**
     * Render toolbar icon via Fluid
     *
     * @return string HTML
     */
    public function getItem(): string
    {
        $view = $this->backendViewFactory->create($this->request, ['codingms/fullscreen']);
        return $view->render('ToolbarItem.html');
    }


    /**
     * Additional attributes of toolbar item
     *
     * @return array List item HTML attibutes
     */
    public function getAdditionalAttributes(): array
    {
        return [
            'class' => 'tx-fullscreen'
        ];
    }

    /**
     * This item has a drop down
     *
     * @return bool
     */
    public function hasDropDown(): bool
    {
        return false;
    }

    /**
     * Position relative to toolbar items
     *
     * @return int
     */
    public function getIndex(): int
    {
        return 10;
    }

    /**
     * Checks whether the user has access to this toolbar item
     *
     * @return bool TRUE if user has access, FALSE if not
     */
    public function checkAccess(): bool
    {
        return true;
    }

    /**
     * Render "drop down" part of this toolbar
     *
     * @return string
     */
    public function getDropDown()
    {
        return '';
    }

    public function setRequest(ServerRequestInterface $request): void
    {
        $this->request = $request;
    }
}
