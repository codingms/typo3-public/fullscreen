# Fullscreen

This extension adds a button to the TYPO3 toolbar to enable and disable the browser fullscreen mode.
