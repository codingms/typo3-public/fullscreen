# Fullscreen Change-Log

## 2024-05-07 Release of version 2.0.1

*	[TASK] Add missing title for mobile menu



## 2024-04-28 Release of version 2.0.0

*	[TASK] Migrate to TYPO3 12 and remove support for TYPO3 11



## 2022-03-16  Release of version 1.0.1

*   [BUGFIX] Fix dependencies in composer.json and emconf



## 2021-11-03  Release of version 1.0.0

*   [TASK] Add editorconfig, gitignore and gitlab-ci files
*   [TASK] Add translations
*   [BUGFIX] Adjust typo3 dependency version in emconf
*   [FEATURE] Add fullscreen toggle button to TYPO3 backend toolbar

