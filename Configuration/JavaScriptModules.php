<?php

return [
    'dependencies' => ['core', 'backend'],
    'imports' => [
        '@codingms/fullscreen/Fullscreen.js' => 'EXT:fullscreen/Resources/Public/JavaScript/Fullscreen.js'
    ],
];
