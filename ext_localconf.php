<?php

if (!defined('TYPO3')) {
    die('Access denied.');
}

call_user_func(
    function ($extKey) {
        $iconsSvg = [
            'icon-fullscreen-deactivate' => 'Resources/Public/Icons/iconmonstr-fullscreen-11.svg',
            'icon-fullscreen-activate' => 'Resources/Public/Icons/iconmonstr-fullscreen-23.svg'
        ];
        $iconRegistry = \TYPO3\CMS\Core\Utility\GeneralUtility::makeInstance(\TYPO3\CMS\Core\Imaging\IconRegistry::class);
        foreach ($iconsSvg as $identifier => $path) {
            $iconRegistry->registerIcon(
                $identifier,
                \TYPO3\CMS\Core\Imaging\IconProvider\SvgIconProvider::class,
                ['source' => 'EXT:fullscreen/' . $path]
            );
        }
    },
    'fullscreen'
);
